
/* Wow Animation*/
new WOW().init();
/* //Wow Animation*/
jQuery('.join-family-slider').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: true
        },
        600: {
            items: 3,
            nav: false
        },
        1000: {
            items: 3,
            nav: true,
            loop: false
        }
    }
})

  $(document).ready(function() {
      $('.owl-carousel.product').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
          0: {
            items: 1,
            nav: true
          },
          600: {
            items: 3,
            nav: false
          },
          1000: {
            items: 4,
            nav: true,
            loop: false,
            margin: 20
          }
        }
      })
    })

/* = For-First-Cap Capital =- */
jQuery('.firstCap').on('keypress', function (event) {
    var $this = $(this),
        thisVal = $this.val(),
        FLC = thisVal.slice(0, 1).toUpperCase();
    con = thisVal.slice(1, thisVal.length);
    jQuery(this).val(FLC + con);
});

/*  FAQ-PAge_TAbing */
jQuery('.faq-section ul.nav.nav-tabs li a').click(function() {
    jQuery('.faq-section ul.nav.nav-tabs li.active').removeClass('active');
    jQuery(this).closest('li').addClass('active');
});

/*  //FAQ-PAge_TAbing */


(function($){
    jQuery(window).on("load",function(){

        jQuery(".form-content").mCustomScrollbar({
            theme:"light-2",
            scrollButtons:{
                enable:true
            },
            callbacks:{
                onTotalScroll:function(){ addContent(this) },
                onTotalScrollOffset:100,
                alwaysTriggerOffsets:false
            }
        });

    });
})(jQuery);

/* -=- Bootstrap-Modal-Multiple =-=- */
jQuery('.modal').on("hidden.bs.modal", function (e) {
    if($('.modal:visible').length)
        {
            jQuery('.modal-backdrop').first().css('z-index', parseInt($('.modal:visible').last().css('z-index')) - 10);
            jQuery('body').addClass('modal-open');
        }
    })
    .on("show.bs.modal", function (e) {
    if($('.modal:visible').length)
    {
        jQuery('.modal-backdrop.in').first().css('z-index', parseInt($('.modal:visible').last().css('z-index')) + 10);
        jQuery(this).css('z-index', parseInt($('.modal-backdrop.in').first().css('z-index')) + 10);
    }
});
/*-== //Bootstrap-Modal-Multiple =---*/


/*  Numeric-Slider */
(function ($) {
    "use strict";

    function customQuantity() {
        /** Custom Input number increment js **/
        jQuery(
            '<div class="pt_QuantityNav"><div class="pt_QuantityButton pt_QuantityUp"><i class="fa fa-angle-up" aria-hidden="true"></i></div><div class="pt_QuantityButton pt_QuantityDown"><i class="fa fa-angle-down" aria-hidden="true"></i></div></div>'
        ).insertAfter(".pt_Quantity input");
        jQuery(".pt_Quantity").each(function () {
            var spinner = jQuery(this),
                input = spinner.find('input[type="number"]'),
                btnUp = spinner.find(".pt_QuantityUp"),
                btnDown = spinner.find(".pt_QuantityDown"),
                min = input.attr("min"),
                max = input.attr("max"),
                valOfAmout = input.val(),
                newVal = 0;

            btnUp.on("click", function () {
                var oldValue = parseFloat(input.val());

                if (oldValue >= max) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue + 1;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });
            btnDown.on("click", function () {
                var oldValue = parseFloat(input.val());
                if (oldValue <= min) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue - 1;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });
        });
    }
    customQuantity();
})(jQuery);


/*  Flexslider  */

/*$('ul.slides li').click(function() {
    $('ul.slides li.flex-active-slide').removeClass('flex-active-slide');
    $(this).closest('li').addClass('flex-active-slide');
});*/

 jQuery(window).load(function () {
    // The slider being synced must be initialized first
    jQuery('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        asNavFor: '#slider'
    });

    jQuery('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
    });
});


/* Card-System-head */
jQuery(document).ready(function() {
    jQuery("div.desc").hide();
    jQuery("input[name$='cars']").click(function() {
        var test = $(this).val();
        jQuery("div.desc").hide();
        jQuery("#" + test).show();
    });
});

jQuery(document).ready(function(){
    jQuery("#media-pen").click(function(){
        jQuery("#media_all").slideToggle("slow");
    });
});
jQuery(".button-gmail_btn").click(function(){
    jQuery("#gmail_form").toggle("slow");
});

/* Card-System-head */

/* Card-Holder-Js */
jQuery('.input-cart-number').on('keyup change', function(){
  $t = $(this);

  if ($t.val().length > 3) {
    $t.next().focus();
  }

  var card_number = '';
  jQuery('.input-cart-number').each(function(){
    card_number += $(this).val() + ' ';
    if ($(this).val().length == 4) {
      $(this).next().focus();
    }
  })

  jQuery('.credit-card-box .number').html(card_number);
});

jQuery('#card-holder').on('keyup change', function(){
  $t = $(this);
  jQuery('.credit-card-box .card-holder div').html($t.val());
});

jQuery('#card-holder').on('keyup change', function(){
  $t = $(this);
  jQuery('.credit-card-box .card-holder div').html($t.val());
});

jQuery('#card-expiration-month, #card-expiration-year').change(function(){
  m = $('#card-expiration-month option').index($('#card-expiration-month option:selected'));
  m = (m < 10) ? '0' + m : m;
  y = $('#card-expiration-year').val().substr(2,2);
  jQuery('.card-expiration-date div').html(m + '/' + y);
})

jQuery('#card-ccv').on('focus', function(){
  jQuery('.credit-card-box').addClass('hover');
}).on('blur', function(){
  jQuery('.credit-card-box').removeClass('hover');
}).on('keyup change', function(){
 jQuery('.ccv div').html($(this).val());
});

setTimeout(function(){
  jQuery('#card-ccv').focus().delay(1000).queue(function(){
    $(this).blur().dequeue();
  });
}, 500);
